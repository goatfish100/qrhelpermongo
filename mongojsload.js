use resources
db.createCollection( "res",
   {
      validator: { $or:
         [
            { uuid: { $type: "string" } },
            { description: { $type: "string" } },
            { name: { $type: "string" } },
            { uuid: { $type: "string" } },
            { protected: { $in: [ "true", "false" ] } },
            { action: { $in: [ "proxy", "redirect" ] } },
            { address: "https://www.yahoo.com"}
         ]
      },
      validationAction: "warn"
   }
)

db.createCollection( "orgusers",
   {
      validator: { $or:
         [
            { uuid: { $type: "string" } },
            { description: { $type: "string" } },
            { uuid: { $type: "string" } },
            { protected: { $in: [ "true", "false" ] } },
            { action: { $in: [ "forward", "redirect", "s3serve", "s3redirect" ] } },
            { address: "https://www.yahoo.com"}
         ]
      },
      validationAction: "warn"
   }
)


var newObjId1 = new ObjectId
var newObjId2 = new ObjectId

db.orgusers.insert(
{
  "_id": newObjId1,
  "orgname": "organization1",
  "address": "organization1",
  "city": "organization1",
  "state": "organization1",
  "postalcode": "organization1",
  "users": [
    {
      "username": "test2",
      "email": "test@yahoo.com",
      "name": "james lapointe",
      "passwd": "hash1"
    },
    {
      "username": "test3",
      "email": "test3@yahoo.com",
      "name": "jose lapointe",
      "passwd": "hash2"
    },
    {
      "username": "test4",
      "email": "test4@yahoo.com",
      "name": "jones lapointe",
      "passwd": "hash2"
    },
    {
      "username": "test5",
      "email": "test5@yahoo.com",
      "name": "jaquene lapointe",
      "passwd": "hash2"
    }
  ]
}
)

db.orgusers.insert(
{
  "_id": newObjId2,
  "orgname": "organization2",
  "address": "organization2",
  "city": "organization2",
  "state": "organization2",
  "postalcode": "organization2",
  "users": [
    {
      "username": "org2test1",
      "email": "test@yahoo.com",
      "name": "fooey shoey",
      "passwd": "hash1"
    },
    {
      "username": "org2test",
      "email": "test3@yahoo.com",
      "name": "jose lapointe",
      "passwd": "hash2"
    }
  ]
}
)


db.res.insert(
  {
  	"uuid": "034c1dd2-d454-11e6-a110-b34e9f2c654a",
  	"description": "test",
    "name": "yahoo resource",
  	"protected": "false",
  	"orgid": newObjId1,
  	"action": "redirect",
  	"address": "https://www.yahoo.com"
  }
  )
  db.res.insert(
{
  	"uuid": "059edd7c-d454-11e6-92b9-374c2fc3d623",
  	"description": "test",
    "name": "yahoo resource",
  	"protected": "false",
  	"orgid": newObjId1,
  	"action": "redirect",
  	"address": "https://www.google.com"
  }
)
db.res.insert(
{
  "uuid": "059edd7c-d454-11e6-92b9-374c2fc3d624",
  "description": "test",
  "name": "yahoo resource",
  "protected": "false",
  "orgid": newObjId1,
  "action": "proxy",
  "address": "https://www.google.com"
}
)
db.res.insert(
 {
  	"uuid": "06953884-d454-11e6-88d7-7f0693261746",
  	"description": "test",
    "name": "yahoo resource",
  	"protected": "false",
  	"orgid": newObjId1,
  	"action": "proxy",
  	"address": "https://www.linkedin.com"
  }
)
db.res.insert(
 {
  	"uuid": "06953884-d454-11e6-88d7-7f0693261747",
  	"description": "test",
    "name": "yahoo resource",
  	"protected": "false",
  	"orgid": newObjId1,
  	"action": "redirect",
  	"address": "https://www.linkedin.com"
  }
)
db.res.insert(
{
  	"uuid": "a9a3ef4c-d455-11e6-8ca2-8bbb8b59273d",
  	"description": "test",
    "name": "yahoo resource",
  	"protected": "false",
  	"orgid": newObjId1,
  	"action": "redirect",
  	"address": "http://localhost:4567/sample-md-file"
  }
)
db.res.insert(
{
  	"uuid": "a9a3ef4c-d455-11e6-8ca2-8bbb8b59273e",
  	"description": "test",
    "name": "yahoo resource",
  	"protected": "false",
  	"orgid": newObjId1,
  	"action": "proxy",
  	"address": "http://localhost:26000/sample-md-file"
  }
)
db.res.insert(
{
  	"uuid": "ac0c5094-d455-11e6-80e5-436a8bb51044",
  	"description": "test",
    "name": "yahoo resource",
  	"protected": "false",
  	"orgid": newObjId1,
  	"action": "redirect",
  	"address": "http://localhost:81/Homesdf"
  }
)
db.res.insert(
{
  	"uuid": "aeb90c38-d455-11e6-83ef-9ba086d72a0a",
  	"description": "test",
    "name": "yahoo resource",
  	"protected": "false",
  	"orgid": newObjId1,
  	"action": "proxy",
  	"address": "https://www.google.com"
  }
)
db.res.insert(
{
  	"uuid": "S3b90c38-d455-11e6-83ef-9ba086d72a0a",
  	"description": "test",
    "name": "yahoo resource",
  	"protected": "false",
  	"orgid": newObjId1,
  	"action": "s3serve",
  	"address": "es-lf51_mul_om.PDF"
}
)
db.res.insert(
{
  	"uuid": "S4b90c38-d455-11e6-83ef-9ba086d72a0a",
  	"description": "test",
    "name": "manual.pdf",
  	"protected": "false",
  	"orgid": newObjId1,
  	"action": "s3redirect",
  	"address": "es-lf51_mul_om.PDF"
}
)
